# myvueapp

> A Vue.js project
Use laravel from repo: https://bitbucket.org/shabbir_sixlogics/vue_laravel.git

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# or for prerendering run
./node_modules/.bin/http-server ./dist/